#!/bin/env python3

# Copyright © 2018 Anton Karmanov <bergentroll@insiberia.net> This program is
# free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.

# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import config
import tkinter
from tkinter import ttk
from lib.menu import Menu
from tkinter import scrolledtext
from lib.tab_main import TabMain
from lib.tab_presets import TabPresets

# Загружаем отступы виджетов из файла конфигурации
padding = config.padding


def quit(event):
    '''
    Функция выхода из программы
    '''
    root.destroy()


def save_as(event):
    '''
    «Сохранить как...» (обёртка над методом меню)
    '''
    menu.save_text_handler()


def update_text(text):
    '''
    Обёртка для обновления текста в главном текстовом поле
    '''
    text_field.configure(state='normal')
    text_field.delete('0.0', tkinter.END)
    text_field.insert(tkinter.INSERT, text)
    text_field.configure(state='disabled')


def tabs_handler(event):
    '''
    Обработчик переключений вкладок
    '''
    selected_tab = notebook.index(notebook.select())
    if selected_tab == notebook.index(tab_main.tab):
        tab_main.refresh_table_view()
    elif selected_tab == notebook.index(tab_presets.tab):
        tab_presets.tune_presets_comment_width()
        tab_presets.presets_list_handler(None)

def copy_text(event):
    '''
    Копирование выделенного в главном текстовом поле текста в буфер
    '''
    text_field.clipboard_clear()
    text_field.clipboard_append(text_field.selection_get())

# Создаём корневое окно
root = tkinter.Tk()

# Пытаемся загрузить значок окна
try:
    icon = tkinter.PhotoImage(file=r'icon.png')
    root.tk.call('wm', 'iconphoto', root._w, icon)
except tkinter._tkinter.TclError:
    pass

# Заголовок окна и пара горячих клавиш
root.title('База данных "Плейлист"')
root.bind_all('<Control-q>', quit)
root.bind_all('<Control-s>', save_as)

# Инициализация меню
menu = Menu(root)

# Инициализация стиля ttk-виджетов
style = ttk.Style()

# Пытаемся выбрать стиль из встроенных
try:
    style.theme_use('clam')
except tkinter.TclError:
    pass

# Основное текстовое поле
text_field = scrolledtext.ScrolledText(
    root,
    font='Monospace',
    wrap='word',
    state='disabled'
)
# Горячая клавиша «Выделить всё»
text_field.bind_all(
    '<Control-a>',
    lambda event: text_field.tag_add('sel', '0.0', tkinter.END)
    )
# Горячая клавиша «Копировать»
text_field.bind_all( '<Control-c>', copy_text)

# Передаём созданный экземпляр текстового поля экземпляру меню
menu.text_field = text_field

# Создаём вкладки
notebook = ttk.Notebook(root)
tab_main = TabMain(notebook, update_text)
tab_presets = TabPresets(notebook, update_text)

# Передаём экземпляру меню метод перезагрузки вкладки пресетов
menu.presets_reload = tab_presets.reload

# Связываем созданные вкладки с родительским виджетом
notebook.add(tab_main.tab, text='Главная')
notebook.add(tab_presets.tab, text='Запросы')

# Если вкладка пресетов инициализировалась с ошибкой, делаем её неактивной
if tab_presets.corrupted:
    notebook.tab(tab_presets.tab, state='disabled')

# Привязываем к виджету вкладок обработчик выбора вкладок
notebook.bind('<<NotebookTabChanged>>', tabs_handler)

# Пакуем виджеты верхнего уровня на корневом окне
notebook.pack(side='left', fill='y', expand=False)
text_field.pack(fill='both', expand=True, padx=padding, pady=padding)

# Запускаем основной цикл, который позволит окну существовать, а не закрываться
# сразу после упаковки
root.mainloop()
