/* Copyright (c) 2018 Anton Karmanov
 This file is a part of undergraduate's thesis. */

CREATE TABLE artists (
  artist_name NVARCHAR(255) PRIMARY KEY NOT NULL,
  artist_year SMALLINT NOT NULL
);

CREATE TABLE songs (
  song_name NVARCHAR(255) NOT NULL,
  artist_name NVARCHAR(255) NOT NULL,
  song_year SMALLINT NOT NULL,
  song_genre NVARCHAR(255) NOT NULL,
  album_name NVARCHAR(255),
  song_rating TINYINT,
  FOREIGN KEY (artist_name) REFERENCES artists (artist_name) ON DELETE CASCADE,
  PRIMARY KEY (song_name, artist_name)
);

CREATE TABLE albums (
  album_name NVARCHAR(255) PRIMARY KEY NOT NULL,
  artist_name NVARCHAR(255),
  album_year SMALLINT NOT NULL,
  FOREIGN KEY (artist_name) REFERENCES artists(artist_name) ON DELETE CASCADE
);

CREATE TABLE awards (
  award_name NVARCHAR(255) PRIMARY KEY NOT NULL,
  song_name NVARCHAR(255) NOT NULL,
  artist_name NVARCHAR(255) NOT NULL,
  FOREIGN KEY (song_name, artist_name) REFERENCES songs (song_name, artist_name)
    ON DELETE CASCADE
);
