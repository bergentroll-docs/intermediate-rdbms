'''
Модуль отвечает за динамическое создание полей ввода.
'''

import tkinter
import config
from tkinter import ttk
from lib.database import Table


class DynamicEntriesException(RuntimeError):
    '''
    Исключение-заглушка для классов в этом модуле
    '''
    pass


class Scrollbox(tkinter.Listbox):
    '''
    Обёртка, создающая виджет поля выбора со скроллбаром
    '''

    def __init__(self, master=None, **kwargs):
        self.frame = ttk.Frame(master)
        super().__init__(self.frame, **kwargs)
        self.scrollbar = ttk.Scrollbar(self.frame, command=self.yview)

    def destroy(self):
        self.scrollbar.destroy()
        super().destroy()
        self.frame.destroy()

    def get(self):
        values = list()
        for index in self.curselection():
            values.append(super().get(index))
        return values

    def pack(self, **kwargs):
        self.frame.pack(**kwargs)
        super().pack(side='left', fill='both', expand=True)
        self.scrollbar.pack(side='right', fill='y', padx=0)


class DynamicEntries():
    '''
    Класс, поддерживающий работу динамических вкладок
    '''
    padding = config.padding

    def __init__(self, entries_dict, ancestor_widget):
        self.labels = dict()
        self.entries = dict()
        self.ancestor_widget = ancestor_widget
        if not entries_dict:
            ancestor_widget.config(height=1)
            return
        for key in entries_dict:
            table = \
                Table(
                    table=entries_dict[key]['table'],
                    field=entries_dict[key]['field']
                    )
            table_body = table.body
            if table.primary_keys.get(key):
                addition = ' (PK):'
            else:
                addition = ':'
            self.labels[key] = ttk.Label(
                ancestor_widget,
                text=key + addition
                )
            values = [val[0] if val[0] else 'NULL' for val in table_body]
            values.sort()
            entrie_type = entries_dict[key].get('type')
            if entrie_type == 'multilist':
                self.entries[key] = Scrollbox(
                    ancestor_widget,
                    selectmode='extended',
                    height=10
                    )
                for i in range(len(values)):
                    self.entries[key].insert(i, values[i])
            elif entrie_type == 'list':
                self.entries[key] = ttk.Combobox(ancestor_widget)
                self.entries[key]['values'] = values
            else:
                raise DynamicEntriesException(
                    'Неверный тип динамического поля: {}'.format(entrie_type)
                )
            self.labels[key].pack(
                fill='both',
                padx=self.padding,
                pady=self.padding
                )
            self.entries[key].pack(
                fill='both',
                padx=self.padding,
                pady=self.padding
                )

    def purge(self):
        '''
        «Ручной» деконструктор класса, в т. ч. удаляет созданные виджеты из
        раскладки окна
        '''
        for key in self.entries:
            self.entries[key].destroy()
        for key in self.labels:
            self.labels[key].destroy()
        self.labels.clear()
        self.entries.clear()
