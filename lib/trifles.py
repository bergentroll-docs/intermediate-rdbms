'''
Модуль с полезныыми функциями, предполагающими множественное использование
'''


def list_to_text(input_list, delimiter):
    '''
    Одинокая функция, подразумевающая множественное использование, создаёт из
    списков строки с разделителями
    '''
    output = str()
    i = len(input_list)
    for item in input_list:
        if i == 1:
            delimiter = ''
        output += item + delimiter
        i -= 1
    return output
