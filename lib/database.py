'''
Это модуль является частью программы playlist.py,
Он предоставляет работу с базой SQLite3
'''

import os
import sys
import config
import sqlite3
from tkinter import messagebox
from lib.trifles import list_to_text


class DatabaseException(RuntimeError):
    '''
    Исключение-заглушка для классов в этом модуле
    '''
    pass


class Database:
    '''
    Класс отвечает за открытие и закрытие базы данных и предоставляет курсор
    для выполнения запросов
    '''

    def __init__(self, filename):
        '''
        Метод обеспечивает подключение к файлу базы данных, первичную настройку
        и получение некоторых данных (список таблиц в базе)
        '''
        if not os.path.isfile(filename):
            messagebox.showerror(
                'Ошибка БД', 'Файла базы {} не существует'.format(filename)
                )
            sys.exit(1)
        self.sqlite_db = sqlite3.connect(filename)
        self.cursor = self.sqlite_db.cursor()
        # Включаем проверку целостности по внешним ключам
        self.cursor.execute("PRAGMA foreign_keys = true")
        self.cursor.fetchall()
        try:
            self.cursor.execute(
                "SELECT name FROM sqlite_master WHERE type='table';"
                )
        except sqlite3.DatabaseError as error:
            messagebox.showerror('Ошибка БД', error)
            sys.exit(1)
        self.tables = self.cursor.fetchall()

    def __del__(self):
        '''
        Деструктор, закрывающий файл базы, когда сборщик мусора обнаружит,
        что не осталось ссылок на экземпляры класса
        '''
        try:
            self.sqlite_db.close()
        except AttributeError:
            pass


class Table:
    '''
    Класс предназначен для извлечения данных и внесения изменений в
    существующие таблицы
    '''
    max_col_width = config.max_col_width
    database = Database(config.database_filename)

    def __init__(self, table=None, query=None, field='*'):
        '''
        Инициализация извлекает данные из таблицы (или таблиц), а так же
        сохраняет схему таблицы (заголовки)
        '''
        if not query:
            query = "SELECT DISTINCT {} FROM {}".format(field, table)
        try:
            self.database.cursor.execute(query)
        except sqlite3.OperationalError as error:
            raise DatabaseException(str(error))
        self.name = table
        self.description = \
            tuple(i[0] for i in self.database.cursor.description)
        self.body = self.database.cursor.fetchall()
        self.get_primary_keys()

    def get_primary_keys(self):
        '''
        Метод создаёт словарь заголовков таблицы, сожержащий информацию,
        является ли поле первичным ключом
        '''
        if not self.name:
            return
        table_info = self.database.cursor.execute(
            'PRAGMA table_info({})'.format(self.name)
        ).fetchall()
        self.primary_keys = {field[1]: field[5] for field in table_info}

    def insert(self, conditions):
        '''
        Вставка новых строк в таблицу
        '''
        if not conditions:
            return
        query = 'INSERT INTO ' + self.name + ' ('
        query += list_to_text(conditions.keys(), ', ')
        query += ')\n'
        query += 'VALUES ('
        query += list_to_text(conditions.values(), ', ')
        query += ')\n'
        try:
            self.database.cursor.execute(query)
            self.database.sqlite_db.commit()
            self.__init__(self.name)
        except sqlite3.IntegrityError as error:
            return 'Ошибка целостности', error
        except sqlite3.OperationalError as error:
            return 'Ошибка операции', error

    def delete(self, conditions):
        '''
        Удаление строк из таблицы
        '''
        if not conditions:
            return
        query = 'DELETE FROM ' + self.name + ' WHERE '
        conditions = \
            ['{} = {}'.format(key, conditions[key]) for key in conditions]
        query += list_to_text(conditions, ' AND ')
        try:
            self.database.cursor.execute(query)
            self.database.sqlite_db.commit()
            self.__init__(self.name)
        except sqlite3.IntegrityError as error:
            return 'Ошибка целостности', error
        except sqlite3.OperationalError as error:
            return 'Ошибка операции', error

    def count_fields_length(self, input):
        '''
        Вспомогательный метод, который находит максимальную длину вхождения
        '''
        fields_length = list()
        for col in range(len(input[0])):
            max_length = len(str(input[0][col]))
            for row in range(1, len(input)):
                if len(str(input[row][col])) > max_length:
                    max_length = len(str(input[row][col]))
            if max_length > self.max_col_width:
                max_length = self.max_col_width
            fields_length.append(max_length)
        return fields_length

    def make_output(self):
        '''
        Метод волшебным образом создаёт из словарей и списков привычное
        человеческому взгляду табличное представление
        '''
        input = [self.description, ] + self.body
        output = str()
        fields_length = self.count_fields_length(input)
        header_delimiter = '|'
        # Make resulting string
        rows_num = len(input)
        for row in range(rows_num):
            cols_num = len(input[row])
            for col in range(cols_num):
                if col == 0:
                    output += '| '
                output += '{:<{width}}'.format(
                        str(input[row][col])[:fields_length[col]],
                        width=fields_length[col]
                        )
                if col == cols_num - 1:
                    output += ' |'
                else:
                    output += ' | '
                if row == 0:
                    header_delimiter += '=' * (fields_length[col] + 2) + '|'
            output += '\n'
            if row == 0:
                output += header_delimiter + '\n'
        return output
