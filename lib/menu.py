'''
Модуль отвечает за компоновку главного меню
'''

import sys
import tkinter
from tkinter import messagebox
from tkinter.filedialog import asksaveasfilename


class Menu:
    EOL = None

    def __init__(self, root):
        self.root = root
        self.menu = tkinter.Menu(root, tearoff='false', relief='flat')
        root.config(menu=self.menu)
        self.menu.add_command(
            label='Сохранить как...', command=self.save_text_handler
        )
        self.menu.add_command(
                label='Перезагрузить пресеты',
                command=lambda: self.presets_reload()
        )
        self.menu.add_command(
            label='Выход', command=lambda: self.root.destroy()
        )
        self.menu.add_command(
            label='О программе',
            command=self.about_handler
        )
        if not self.EOL:
            self.set_eol()

    def set_eol(self):
        '''
        Метод задаёт окончание строки взависимости от операционной системы
        '''
        platform = sys.platform
        if platform == 'win32':
            self.EOL = '\r\n'
        elif platform == 'darwin':
            self.EOL = '\r'
        else:
            self.EOL = '\n'

    def about_handler(self):
        info = (
            'Данная программа является частью курсовой работы Антона '
            'Карманова <bergentroll@insiberia.net> за 2018 год, '
            'выпущена под лицензией GPL3+ и может распространяться без '
            'ограничений согласно применённой лицензии.\n'
            '\nПолучилось неплохо, я горжусь собой.'
            )
        messagebox.showinfo('О программе', info)

    def save_text_handler(self):
        '''
        Метод отвечает за вызов диалогового окна соханения файла и сохраняет в
        фай содержимое основного текстового поля
        '''
        file_path = asksaveasfilename(
            parent=self.root,
            defaultextension='.txt',
            filetypes=(('Текст', '.txt'),),
            title='Сохранить таблицу как...'
        )
        if not file_path:
            return
        with open(file_path, mode='w', newline=self.EOL) as save_file:
            save_file.write(self.text_field.get('0.0', tkinter.END))

    def presets_reload():
        '''
        Метод должен быть переназначен в главном модуле
        '''
        pass
