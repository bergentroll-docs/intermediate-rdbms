'''
Данный модуль отвечает за работу влкадки «Запросы»
'''

import config
import tkinter
from tkinter import ttk
from tkinter import messagebox
from lib.trifles import list_to_text
from lib.database import Table, DatabaseException
from lib.presets import Presets, PresetsException
from lib.dynamic_entries import DynamicEntries, DynamicEntriesException


class TabPresets:
    padding = config.padding

    def update_text():
        '''
        Этот метод должен быть переназначен в главном модуле
        '''
        pass

    def __init__(self, ancestor, update_text):
        self.update_text = update_text
        self.ancestor = ancestor
        self.presets = dict()
        self.corrupted = False
        try:
            self.presets = Presets().dictionary
        except PresetsException as error:
            messagebox.showerror('Ошибка разбора пресетов', error)
            self.corrupted = True
        self.tab = ttk.Frame(ancestor, relief='flat')
        self.presets_select_frame = ttk.LabelFrame(
            self.tab,
            relief='ridge',
            text='Выбор пресета'
            )
        self.presets_list = ttk.Combobox(
            self.presets_select_frame,
            state='readonly'
            )
        if self.presets:
            self.presets_list['values'] = tuple(self.presets)
            self.presets_list.current(0)
        self.presets_list.bind(
                '<<ComboboxSelected>>',
                self.presets_list_handler
                )
        self.presets_comment_label = tkinter.Label(
                self.presets_select_frame,
                justify=tkinter.LEFT,
                )
        self.presets_control_frame = ttk.LabelFrame(
                self.tab,
                relief='ridge',
                text='Настройка пресета'
                )
        self.preset_dynamic_entries_frame = \
            ttk.Frame(
                self.presets_control_frame,
                relief='flat'
                )
        self.presets_apply_button = ttk.Button(
            self.presets_control_frame,
            text='Применить',
            command=self.presets_apply_button_handler
            )
        if not self.presets:
            self.presets_apply_button.config(state='disabled')
        self.dynamic_entries_init()
        self.pack()

    def reload(self):
        '''
        Этот метод используется для ручной перезагрузки данных вкладки в
        случае, если пользователь изменил файл пресетов
        '''
        try:
            self.presets = Presets().dictionary
            self.corrupted = False
            self.ancestor.tab(self.tab, state='normal')
        except PresetsException as error:
            messagebox.showerror('Ошибка разбора пресетов', error)
            self.corrupted = True
            self.ancestor.tab(self.tab, state='disabled')
        if self.presets:
            self.presets_list['values'] = tuple(self.presets)
            self.presets_list.current(0)
            self.dynamic_entries_init()

    def dynamic_entries_init(self):
        '''
        Инициализация динамических виджетов
        '''
        if not self.presets:
            return
        try:
            entries = \
                self.presets[self.presets_list.get()]['entries']
            self.dynamic_entries = \
                DynamicEntries(
                    entries,
                    self.preset_dynamic_entries_frame
                    )
        except DynamicEntriesException as error:
            messagebox.showerror('Разбор динамических полей', error)
            self.presets_apply_button.config(state='disabled')

    def presets_list_handler(self, event):
        '''
        Обработчик списка пресетов
        '''
        if not self.presets_list.get():
            return
        self.presets_apply_button.config(state='active')
        self.preset = self.presets[self.presets_list.get()]
        self.tune_presets_comment_width(self.preset['comment'])
        if self.dynamic_entries:
            self.dynamic_entries.purge()
        self.dynamic_entries_init()

    def tune_presets_comment_width(self, text=None):
        '''
        Метод выравнивает ширину контейнера при изменении текста комментария
        '''
        wraplength = self.presets_select_frame.winfo_width() - 4 * self.padding
        self.presets_comment_label.config(wraplength=wraplength)
        if text:
            self.presets_comment_label.config(text=text)

    def presets_apply_button_handler(self):
        '''
        Обработчик кнопки применения пресета, запускает обращение к базе
        '''
        query = self.preset['query']
        substitutes = list()
        entries = self.preset['entries']
        dynamic_entries = self.dynamic_entries.entries
        for key in entries.keys():
            if entries[key]['type'] == 'list':
                substitutes.append(
                    '\'{}\''.format(
                        dynamic_entries[key].get()
                        )
                    )
            elif entries[key]['type'] == 'multilist':
                values = tuple(
                    '\'{}\''.format(i) for i in dynamic_entries[key].get()
                    )
                string = list_to_text(values, entries[key]['delimiter'])
                substitutes.append(string)
        query = query.format(*(substitutes))
        try:
            self.table = Table(query=query)
            self.update_text(self.table.make_output())
        except DatabaseException as error:
            messagebox.showerror('Неверный запрос', error)
        except TypeError as error:
            messagebox.showerror('Ошибка типов', error)

    def pack(self):
        '''
        В этот метод вынесена упаковка виджетов на родительском виджете
        '''
        self.presets_select_frame.pack(
            fill='x',
            padx=self.padding,
            pady=self.padding
            )
        self.presets_list.pack(
            fill='x',
            padx=self.padding,
            pady=self.padding
            )
        self.presets_comment_label.pack(
            fill='x',
            expand=False,
            padx=self.padding,
            pady=self.padding
            )
        self.presets_control_frame.pack(
            fill='x',
            padx=self.padding,
            pady=self.padding
            )
        self.preset_dynamic_entries_frame.pack(
            fill='x'
            )
        self.presets_apply_button.pack(
            padx=self.padding,
            pady=self.padding
            )
