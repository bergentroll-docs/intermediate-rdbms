'''
Модуль отвечает за загрузку файла пресетов
'''

import os
import json
import config
from lib.trifles import list_to_text


class PresetsException(RuntimeError):
    '''
    Исключение-заглушка для классов в этом модуле
    '''
    pass


class Presets:
    '''
    При инициализации объект пытается открыть файл пресетов и разобрать
    json-структуры, получив понятные Python словари и списки
    '''
    presets_filename = config.presets_filename
    dictionary = dict()

    def __init__(self):
        if not os.path.isfile(self.presets_filename):
                open(self.presets_filename, 'a').close()
        if os.stat(self.presets_filename).st_size > 0:
            with open(self.presets_filename, 'r') as presets_file:
                try:
                    self.dictionary = json.load(presets_file)
                except json.decoder.JSONDecodeError as error:
                    raise PresetsException(
                            'В файле {}:\n {}'.format(
                                self.presets_filename,
                                str(error)
                                )
                            )
        self.normalize_queries()

    def normalize_queries(self):
        '''
        Метод приводит SQL-запросы и комментарии, хранящиеся в виде массивов, в
        строчное представление
        '''
        for key in self.dictionary:
            self.dictionary[key]['query'] = \
                list_to_text(self.dictionary[key]['query'], ' ')
            self.dictionary[key]['comment'] = \
                list_to_text(self.dictionary[key]['comment'], ' ')
